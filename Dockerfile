FROM gradle:8.8-jdk21-jammy AS build
ADD . .
RUN gradle --no-daemon --no-scan --no-build-cache distTar && \
  mkdir /dist && \
  tar xf $(find ./build/distributions/ -name *.tar | head -1) && \
  mv app /dist

FROM eclipse-temurin:21-jre-jammy
RUN apt-get update && \
    apt-get install -y python3 && \
    apt-get clean
COPY --from=build /dist/ /
RUN mkdir "/input"
COPY data/srf_029_call_number_translation_data.csv /input/srf_029_call_number_translation_data.csv
COPY data/srf-041_PlayURN_transactional_data.csv /input/srf-041_PlayURN_transactional_data.csv
CMD ["/app/bin/json-data-transform"]