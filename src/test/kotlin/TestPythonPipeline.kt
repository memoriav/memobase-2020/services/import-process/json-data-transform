import ch.memobase.KafkaTopology
import ch.memobase.PropertyName
import ch.memobase.Reports
import ch.memobase.Service
import ch.memobase.reporting.Report
import ch.memobase.testing.EmbeddedSftpServer
import kotlinx.serialization.json.Json
import org.apache.kafka.common.header.internals.RecordHeader
import org.apache.kafka.common.header.internals.RecordHeaders
import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.kafka.common.serialization.StringSerializer
import org.apache.kafka.streams.TestOutputTopic
import org.apache.kafka.streams.TopologyTestDriver
import org.apache.kafka.streams.test.TestRecord
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.assertAll
import java.io.File
import java.nio.charset.Charset

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestPythonPipeline {
    private val resourcePath = "src/test/resources/integration/srg-api"
    private fun readFile(fileName: String, count: Int): String {
        return File("$resourcePath/$count/$fileName").readText(Charset.defaultCharset())
    }

    @BeforeAll
    fun startSftpServer() {
        EmbeddedSftpServer(22001, "user", "password")
    }

    private val reports = Reports("test", "testVersion")

    private fun setupTest(
        count: Int,
        inputKey: String
    ): Pair<TestOutputTopic<String, String>, TestOutputTopic<String, String>> {
        val settings = Service.load("integration/srg-api/$count/app.yml")
        val testDriver =
            TopologyTestDriver(KafkaTopology(settings).build(), settings.kafkaStreamsSettings)
        val inputValue = readFile("input.json", count)
        val headers = RecordHeaders()
        headers.add(RecordHeader("sessionId", "test-session-id".toByteArray()))
        headers.add(RecordHeader("recordSetId", "tst-001".toByteArray()))
        headers.add(RecordHeader("institutionId", "ins".toByteArray()))
        val inputTopic =
            testDriver.createInputTopic(
                settings.appSettings.getProperty(PropertyName.SRG_API_INGEST_TOPIC),
                StringSerializer(),
                StringSerializer()
            )
        val configTopic = testDriver.createInputTopic(
            settings.appSettings.getProperty(PropertyName.CONFIG_TOPIC_NAME),
            StringSerializer(),
            StringSerializer()
        )
        val configRecord = TestRecord("tst-001#pythonTransform", readFile("pythonTransform.py", count))
        configTopic.pipeInput(configRecord)
        val inputRecord = TestRecord(inputKey, inputValue, headers)
        inputTopic.pipeInput(inputRecord)
        val outputTopic =
            testDriver.createOutputTopic(settings.outputTopic, StringDeserializer(), StringDeserializer())
        val outputReportTopic =
            testDriver.createOutputTopic(
                settings.processReportTopic,
                StringDeserializer(),
                StringDeserializer()
            )
        return Pair(outputTopic, outputReportTopic)
    }


    @Test
    fun `test 1 valid json input`() {
        val topics = setupTest(1, "test-1")
        val record = topics.first.readRecord()
        val recordValue = record.value
        val recordKey = record.key

        val report = topics.second.readRecord()
        val reportValue = Json.decodeFromString<Report>(report.value)
        val reportKey = report.key

        assertAll(
            {
                assertThat(recordValue)
                    .isEqualTo(readFile("output.json", 1))
            },
            {
                assertThat(recordKey)
                    .isEqualTo("test-1")
            },
            {
                assertThat(reportValue)
                    .isEqualTo(reports.success("test-1", "All records were generated without warnings! Total Number of Records: 1."))
            },
            {
                assertThat(reportKey)
                    .isEqualTo("test-1")
            }
        )
    }

    @Test
    fun `test 2 empty script file input`() {
        val topics = setupTest(2, "test-2")
        assertThat(topics.second.isEmpty)
            .withFailMessage("Reports topic is empty.")
            .isFalse
        assertThat(topics.first.isEmpty)
            .withFailMessage("Records topic is empty.")
            .isFalse

        val record = topics.first.readRecord()
        val recordValue = record.value
        val recordKey = record.key

        val report = topics.second.readRecord()
        val reportValue = Json.decodeFromString<Report>(report.value)
        val reportKey = report.key

        assertAll(
            {
                assertThat(reportValue.status)
                    .isEqualTo("WARNING")
            },
            {
                assertThat(reportValue.message)
                    .isEqualTo("No changes made to the JSON file.")
            },
            {
                assertThat(reportKey)
                    .isEqualTo("test-2")
            },
            {
                assertThat(recordKey)
                    .isEqualTo("test-2")
            },
            {
                assertThat(recordValue)
                    .isEqualTo(readFile("output.json", 2))
            }
        )
    }
}