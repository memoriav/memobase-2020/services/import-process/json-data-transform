import ch.memobase.PythonHandler
import ch.memobase.Reports
import ch.memobase.reporting.Report
import ch.memobase.reporting.ReportStatus
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.assertAll
import java.io.File
import java.nio.charset.Charset

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestPythonHandler {
    private val resourcePath = "src/test/resources/python"

    private val reports = Reports("test", "testVersion")
    private fun readFile(fileName: String): String {
        return File("$resourcePath/$fileName").readText(Charset.defaultCharset())
    }

    @Test
    fun `test 1 valid python input`() {
        val script = readFile("script_1.py")
        val message = readFile("data_script_1.json")
        val data = ""
        val pythonHandler = PythonHandler(reports, "input", "output")
        val output = pythonHandler.transform("test-key", script, data, message, "tst", "tst-001")

        assertAll(
            "",
            {
                assertThat(output.results)
                    .isEqualTo(listOf("{\"data\":\"test\",\"json\":\"works!\"}"))
            },
            {
                assertThat(output.report)
                    .isEqualTo(
                        reports.success(
                            "test-key",
                            "All records were generated without warnings! Total Number of Records: 1."
                        )
                    )
            }
        )
    }

    @Test
    @Disabled
    fun `test 2 srg script`() {
        val script = readFile("script_2_srg_test.py")
        val message = readFile("data_script_2.json")
        val data = ""
        val pythonHandler = PythonHandler(reports, "input", "output")
        val output = pythonHandler.transform("test-key", script, data, message, "srf", "srf-029")
        assertAll(
            "",
            {
                assertThat(output.results.toString())
                    .isEqualTo(readFile("json_output_2.txt"))
            },
            {
                assertThat(output.report)
                    .isEqualTo(reports.warning("test-key", readFile("report_message_2.txt")))
            }
        )
    }


    @Test
    fun `test 3 invalid json input`() {
        val script = readFile("script_3.py")
        val message = readFile("data_script_3.json")
        val data = ""
        val pythonHandler = PythonHandler(reports, "input", "output")
        val output = pythonHandler.transform("test-key", script, data, message, "tst", "tst-001")

        assertAll(
            "",
            {
                assertThat(output.results)
                    .isEqualTo(emptyList<String>())
            },
            {
                assertThat(output.report)
                    .isEqualTo(
                        reports.fatal(
                            "test-key",
                            "The python script did not produce any records from the input message.\n" +
                                    "Error Messages:\n" +
                                    "INVALID JSON (Could not deserialize input as JSON): Invalid control character at: line 2 column 15 (char 16).\n",

                            )
                )
            }
        )
    }

    @Test
    fun `test 4 invalid python script`() {
        val script = readFile("script_4.py")
        val message = readFile("data_script_4.json")
        val data = ""
        val pythonHandler = PythonHandler(reports, "input", "output")
        val output = pythonHandler.transform("test-key", script, data, message, "tst", "tst-001")

        assertAll(
            "",
            {
                assertThat(output.results)
                    .isEqualTo(emptyList<String>())
            },
            {
                // the output message for this report changes depending on environment as it contains the path to the python script.
                assertThat(output.report.status)
                    .isEqualTo(ReportStatus.fatal)
            }
        )
    }
}