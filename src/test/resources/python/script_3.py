### JSON DATA TRANSFORM TEMPLATE
# Do not change any of the exisiting template code. Otherwise it will likely break.
import json
import os
import sys
import logging

from json import JSONDecodeError

# Only add imports which are added by the standard library. 
# Other libraries need to be added to the environment.
### CUSTOM IMPORTS START

### CUSTOM IMPORTS END


### READ ENV VARIABLES
input_folder = os.environ['SCRIPT_INPUT_FOLDER']
output_folder = os.environ['SCRIPT_OUTPUT_FOLDER']

### ADD LOG LINES TO IMPLEMENT WARNINGS OR FATAL ERRORS.
logging.basicConfig(stream=sys.stdout, level=logging.INFO, format='%(message)s')

def transformation(inputDocument, institutitionId, recordSetId):
    # Implement transformations here. Either in place or return a new dictionary with the 
    # desired structure.
    ### CUSTOM CODE START
    with open(os.path.join(output_folder, 'file.json'), 'w') as fp:
        json.dumps(inputDocument, fp, ensure_ascii=False, indent=None, separators=(',', ':'))
    ### CUSTOM CODE END

### TEMPLATE WRAPPER CODE. DO NOT CHANGE THIS.
def main(message: str, institutionId: str, recordSetId: str):
    try:
        document = json.loads(message)
        try:
            transformation(document, institutionId, recordSetId)
        except Exception as er:
            logging.error(er)
            exit(1)
    except JSONDecodeError as er:
        logging.error(f'INVALID JSON (Could not deserialize input as JSON): {er}.')
        exit(1)


if __name__ == '__main__':
    with open(sys.argv[1], 'r') as fp:
        message = fp.read()
    main(message, sys.argv[2], sys.argv[3])
