### JSON DATA TRANSFORM TEMPLATE
# Do not change any of the exisiting template code. Otherwise it will likely break.
import json
import sys
import logging
import os

from json import JSONDecodeError

# Only add imports which are added by the standard library. 
# Other libraries need to be added to the environment.
### CUSTOM IMPORTS START
import re
### CUSTOM IMPORTS END


### CUSTOM HELPER FUNCTIONS START
# Function to convert milliseconds to hours:minutes:seconds format
def convert_from_ms( milliseconds ): 
    seconds, milliseconds = divmod(milliseconds,1000) 
    minutes, seconds = divmod(seconds, 60) 
    hours, minutes = divmod(minutes, 60) 
    days, hours = divmod(hours, 24) 
    seconds = seconds + milliseconds/1000 
    return days, hours, minutes, seconds
### CUSTOM HELPER FUNCTIONS END

### READ ENV VARIABLES
input_folder = os.environ['SCRIPT_INPUT_FOLDER']
output_folder = os.environ['SCRIPT_OUTPUT_FOLDER']

### DO NOT ADD ANY CUSTOM LOGGING LINES. OTHERWISE IT WILL BREAK THE INTEGRATION!
logging.basicConfig(stream=sys.stdout, level=logging.INFO, format='%(message)s')

def transformation(inputDocument, institutitionId, recordSetId):
    # Implement transformations here. Either in place or return a new dictionary with the 
    # desired structure.
    ### CUSTOM CODE START
    edge = inputDocument        
    MBRecord = {}
    
    # INFORMATION ABOUT COLLECTION (edge isMemberOf Collection)
    MBRecord["collectionID"] = edge["isMemberOf"]["identifier"]
    MBRecord["collectionPublisher"] = edge["isMemberOf"]["hasPublisher"]
    MBRecord["collectionTitle"] = edge["isMemberOf"]["title"]
    #MBRecord["collectionAbstract"] = edge["isMemberOf"]["abstract"]
    #MBRecord["collectionDescription"] = edge["isMemberOf"]["description"]
    #MBRecord["collectionGenre"] = edge["isMemberOf"]["hasGenre"]...to be completed in case
    #MBRecord["collectionRelatedPicture"] = edge["isMemberOf"]["hasRelatedPicture"]...to be completed in case

    
    # INFORMATION ABOUT PROGRAMME (edge)
    MBRecord["issueID"] = edge["identifier"]
    MBRecord["issueAssetType"] = edge["assetType"]
    #MBRecord["issueComments"] = edge["comments"]
    #MBRecord["issueTitle"] = edge["title"] # not used, seems wrong
    #MBRecord["issueAbstract"] = edge["abstract"]
    #MBRecord["issueDescription"] = edge["description"]
    #MBRecord["issueDateArchived"] = edge["dateArchived"]
    #MBRecord["issueDateModified"] = edge["dateModified"]
    # Transform dateBroadcast (remove T00:00:00z) ° @UB: is this necessary?
    try:
        if edge["dateBroadcast"] is not None and edge["dateBroadcast"].endswith('T00:00:00'):
            MBRecord["issueDateBroadcast"] = edge["dateBroadcast"][:-9]
    except KeyError:
        logging.warning("No issueDateBroadcast for " + recordSetId + '_' + edge["identifier"])
    #MBRecord["issueDateProduced"] = edge["dateProduced"]
    MBRecord["issueDuration"] = edge["duration"]
    #MBRecord["issueUsageRights"] = edge["hasUsageRights"]
    #MBRecord["issueProductionType"] = edge["hasProductionType"]
    #MBRecord["issueRelatedLocation"] = edge["hasRelatedLocation"]
    #MBRecord["issueRelatedEvent"] = edge["hasRelatedEvent"]
    #MBRecord["issueProducer"] = edge["hasProducer"]
    MBRecord["issueTopic"] = edge["hasTopic"]
    #MBRecord["issuePosition"] = edge["position"]
    #MBRecord["issueCoveredBy"] = edge["isCoveredBy"]
    
    #MBRecord["issueGenre"] = edge["hasGenre"]...to be completed in case
    #MBRecord["issueRelatedPicture"] = edge["hasRelatedPicture"]...to be completed in case
    #MBRecord["issueRelatedResource"] = edge["hasRelatedResource"]...to be completed in case
    #MBRecord["issueSchedule"] = edge["isScheduledOn"]...to be completed in case

    # Split contributors into Persons and Organisations (persons, corporateBody, agent)
    MBRecord["personContributor"] = []
    MBRecord["orgContributor"] = []
    for contributor in edge["hasContributor"]:
        # Person Contributors
        if contributor["hasType"] == "Person":
            # Case 1: givenName + familyName
            if "familyName" in contributor and contributor["familyName"] is not None and "givenName" in contributor and contributor["givenName"] is not None:
                contributor["name"] = contributor["givenName"] + " " + contributor["familyName"]
                del contributor["familyName"]
                del contributor["givenName"]
            # Case 2: only familyName (no givenName)
            if "familyName" in contributor and contributor["familyName"] is not None and contributor["givenName"] is None:
                contributor["name"] = contributor["familyName"]
                del contributor["familyName"]
                del contributor["givenName"]
            MBRecord["personContributor"].append(contributor)
        # Organisation contributors
        elif contributor["hasType"] == "Department" or contributor["hasType"] == "Team":
            MBRecord["orgContributor"].append(contributor)
        else:
            logging.warning("Different contributor's type (" + contributor["hasType"] + ") in " + recordSetId + '_'+ edge["identifier"])
        # Concatenate roles
        if "hasRole" in contributor:
            if len(contributor["hasRole"]) == 1:
                role = ''.join(contributor["hasRole"])
                contributor["hasRole"] = role # ° check if role added to MBRecord contributors
            elif len(contributor["hasRole"]) > 1:
                logging.warning("More than one contributor's role in " + recordSetId + '_'+ edge["identifier"])
#    del edge["hasContributor"] # ° check if needed
    
    # Programme related editorial object
    try:
        if 0 in range(-len(edge["hasRelatedEditorialObject"]), len(edge["hasRelatedEditorialObject"])):
            MBRecord["issueEpisodeURN"] = edge["hasRelatedEditorialObject"][0]["identifier"]
            MBRecord["issueTitle"] = edge["hasRelatedEditorialObject"][0]["title"]
            MBRecord["issueVideoURN"] = edge["hasRelatedEditorialObject"][0]["hasMember"][0]["identifier"]
        else:
            logging.warning("No relatedEditorialObject for " + recordSetId + '_'+ edge["identifier"])    
            if MBRecord["issueDateBroadcast"] is not None:
                MBRecord["issueTitle"] = MBRecord["collectionTitle"] + " " + MBRecord["issueDateBroadcast"][-2:] + "." + MBRecord["issueDateBroadcast"][5:7] + "." + MBRecord["issueDateBroadcast"][0:4]
    except KeyError:
        logging.warning("No relatedEditorialObject for " + recordSetId + '_'+ edge["identifier"])

    #MBRecord["issueInstance"] = edge["isInstantiatedBy"]...to be completed in case (not needed as same info as in hasMember/isIstnantiatedBy)
    #MBRecord["issuePart"] = edge["hasPart"]...to be completed in case

    
    # INFORMATION ABOUT ITEM (edge hasMember Item)
    
    # CASE 1: NO MEMBERS in hasMember => level Programme (issue/episode)
    if not edge["hasMember"]:
        logging.warning("No items for Programme " + recordSetId + '_'+ edge["identifier"])

        MBRecord["MemobaseID"] = MBRecord["issueID"] # SSR SRG ID for episode

        # Convert duration
        if MBRecord["issueDuration"] is not None:
            milliseconds = int(MBRecord["issueDuration"])
            time = convert_from_ms(milliseconds)
            # Add missing leading 0
            hours_str = str(time[1])
            min_str = str(time[2])
            sec_str = "{:.0f}".format(time[3])                    
            zero_filled_hours = hours_str.zfill(2)
            zero_filled_min = min_str.zfill(2)
            zero_filled_sec = sec_str.zfill(2)
            MBRecord["itemDuration"] = zero_filled_hours + ":" + zero_filled_min + ":" + zero_filled_sec

        # Streams URN
        for EdgeRelEdObj in edge["hasRelatedEditorialObject"]:
            if "hasMember" in EdgeRelEdObj:
                if len(EdgeRelEdObj["hasMember"]) == 1: #° TO CHECK: why I do this? because first is always the Program
                    MBRecord["streamingLink"] = EdgeRelEdObj["hasMember"][0]["identifier"]
                elif len(EdgeRelEdObj["hasMember"]) == 2:
                    MBRecord["streamingLink"] = EdgeRelEdObj["hasMember"][1]["identifier"]
                else:
                    logging.warning("There are " + str(len(EdgeRelEdObj["hasMember"])) + "members for " + recordSetId + '_'+ edge["identifier"])

        # Add technical remarks
        MBRecord["technicalRemarks"] = "Programm ID: " + MBRecord["issueID"] + "<br/>Item ID: " + MBRecord["issueID"]

        # Write identifiers' list ° TODO

        # Set access
        try:
            if MBRecord["streamingLink"] is not None:
                MBRecord["access"] = "public"
                # Add backlink
                MBRecord["references"] = "https://www.rts.ch/play/radio/quicklink/" + MBRecord["streamingLink"][14:]# urn:rts:audio:3297854
            else:
                MBRecord["access"] = "onsite"
        except KeyError:
            logging.warning("No streaming link for " + recordSetId + '_'+ edge["identifier"])

        ### OUTPUT DEFINITION
        with open(os.path.join(output_folder, f'{recordSetId}_{MBRecord["MemobaseID"]}.json'), 'w') as fp:
            json.dump(MBRecord, fp, ensure_ascii=False, indent=None, separators=(',', ':'))


    # CASE 2: MEMBERS in hasMember => level Item (Beitrag)
    else:
        for item in edge["hasMember"]:
            MBRecord["itemID"] = item["identifier"]
            MBRecord["itemComments"] = item["comments"]
            MBRecord["itemTitle"] = item["title"]
            
            # Format item abstract
            if item["abstract"] is not None:
                MBRecord["itemAbstract"] = item["abstract"].replace('\\', '')
                MBRecord["itemAbstract"] = MBRecord["itemAbstract"].replace(' <','<br/>')
                # Two cases: with or without < (<br/>) # ° TODO: fix cases like https://stage.memobase.ch/de/object/rtr-003-6d929e8a-0dd1-4b7d-992e-d4b932c26e14_02                    
                m = re.search('^(.+?)<br/>', MBRecord["itemAbstract"])
                MBRecord["itemRecordingLocation"] = []
                if m:
                    country = m.group(1)
                    places = re.findall('<br/>(.+?):', MBRecord["itemAbstract"])
                    if places:
                        for place in places:                            
                            place = country + ", " + place
                            if place.startswith("Schweiz, "):
                                place = place[len("Schweiz, "):]
                            MBRecord["itemRecordingLocation"].append(place)
                else:
                    place = re.search('^(.+?):', MBRecord["itemAbstract"])
                    if place:
                        place = place.group(1)
                        if place.startswith("Schweiz, "):
                            place = place[len("Schweiz, "):]
                        MBRecord["itemRecordingLocation"].append(place)
            else:
                logging.warning("No abstract for: " + recordSetId + '_'+ edge["identifier"]  + '_' + MBRecord["itemID"])

            #MBRecord["itemDateArchived"] = item["dateArchived"]
            #MBRecord["itemDateModified"] = item["dateModified"]

            MBRecord["itemDescription"] = item["description"]
            
            # Convert duration
            if item["duration"] is not None:
                milliseconds = int(item["duration"])
                time = convert_from_ms(milliseconds)
                # Add missing leading 0
                hours_str = str(time[1])
                min_str = str(time[2])
                sec_str = "{:.0f}".format(time[3])
                zero_filled_hours = hours_str.zfill(2)
                zero_filled_min = min_str.zfill(2)
                zero_filled_sec = sec_str.zfill(2)
                MBRecord["itemDuration"] = zero_filled_hours + ":" + zero_filled_min + ":" + zero_filled_sec

            MBRecord["itemStart"] = item["start"]
            MBRecord["itemEnd"] = item["end"]

            MBRecord["itemUsageRights"] = item["hasUsageRights"]
            MBRecord["itemUsageRestrictions"] = item["hasUsageRestrictions"]
            try:
                MBRecord["itemAccessConditions"] = item["hasAccessConditions"]
            except KeyError:
                logging.warning("There are no AccessConditions for " + recordSetId + '_'+ edge["identifier"]  + '_' + MBRecord["itemID"])    
            #MBRecord["itemRightsHolder"] = item["isCoveredBy"]...to be completed in case

            MBRecord["itemProductionType"] = item["hasProductionType"]
            MBRecord["itemProducer"] = item["hasProducer"]
            MBRecord["itemRelatedLocation"] = item["hasRelatedLocation"]

            MBRecord["itemPosition"] = item["position"]
            positionInt = int(MBRecord["itemPosition"])+1
            positionNorm = str(positionInt).zfill(2)
            MBRecord["MemobaseID"] = MBRecord["issueID"] + "_" + positionNorm

            # Clean topics
            topics = []
            for topic in item["hasTopic"]:
                topic = topic[1:].replace('.¦', ', ')
                topic = topic.rstrip(topic[-1])
                topic = topic.replace('¦', ', ')
                #topic = topic + ";"
                topic = topic.title()
                topics.append(topic)
            MBRecord["itemTopic"] = topics

            #MBRecord["itemRelatedResource"] = item["hasRelatedResource"]...to be completed in case
            #MBRecord["itemRelatedPicture"] = item["hasRelatedPicture"]...to be completed in case
            
            if 0 in range(-len(item["hasRelatedEditorialObject"]), len(item["hasRelatedEditorialObject"])):
                if item["hasRelatedEditorialObject"][0]["identifier"] is not None:
                    MBRecord["itemURN"] = item["hasRelatedEditorialObject"][0]["identifier"]
                elif len(edge["hasRelatedEditorialObject"]) == 1 and len(edge["hasRelatedEditorialObject"][0]["hasMember"]) == 1 and edge["    hasRelatedEditorialObject"][0]["hasMember"][0]["title"] == item["title"]:
                    MBRecord["itemURN"] = edge["hasRelatedEditorialObject"][0]["hasMember"][0]["identifier"]
                else:
                    logging.warning("No item URN for " + recordSetId + '_'+ edge["identifier"]  + '_' + MBRecord["itemID"])
            else:
                logging.warning("No related editorial object for item " + recordSetId + '_'+ edge["identifier"]  + '_' + MBRecord["itemID"])

            #try:
            #    MBRecord["itemURN"] = item["hasRelatedEditorialObject"][0]["identifier"]
            #except IndexError:
            #    logging.warning("No identifier for " + recordSetId + '_'+ edge["identifier"]  + '_' + MBRecord["itemID"])
            #MBRecord["itemTitle"] = item["hasRelatedEditorialObject"][0]["title"] # not needed, should be the same as item["title"]
                    
            # Split contributors into Persons and Organisations (persons, corporateBody, agent)
            for contributor in item["hasContributor"]:
                if contributor["hasType"] == "Person":
                    # Case 1: givenName + familyName
                    if "familyName" in contributor and contributor["familyName"] is not None and "givenName" in contributor and contributor["givenName"] is not None:
                        contributor["name"] = contributor["givenName"] + " " + contributor["familyName"]
                        del contributor["familyName"]
                        del contributor["givenName"]
                    # Case 2: only familyName (no givenName)
                    if "familyName" in contributor and contributor["familyName"] is not None and contributor["givenName"] is None:
                        contributor["name"] = contributor["familyName"]
                        del contributor["familyName"]
                        del contributor["givenName"]
                    MBRecord["personContributor"].append(contributor)
                elif contributor["hasType"] == "Department" or contributor["hasType"] == "Team":
                    MBRecord["orgContributor"].append(contributor)
                else:
                    logging.warning("Different contributor's type (" + contributor["hasType"] + ") for " + recordSetId + '_'+ edge["identifier"]  + '_' + MBRecord["itemID"])
            # Concatenate roles
                if "hasRole" in contributor:
                    if len(contributor["hasRole"]) == 1:
                        role = ''.join(contributor["hasRole"])
                        contributor["hasRole"] = role
                    elif len(contributor["hasRole"]) > 1:
                        logging.warning("More than one role for contributor in " + recordSetId + '_'+ edge["identifier"]  + '_' + MBRecord["itemID"])
            #del item["hasContributor"] # ° Check if needed

            # Instances
            MBRecord["itemInstance"] = []
            for instance in item["isInstantiatedBy"]:
                try:
                    instance["audioTrackID"] = instance["hasAudioTrack"][0]["identifier"] # ° check if there can be more than one
                    instance["audioTrackLanguage"] = instance["hasAudioTrack"][0]["hasLanguage"]
                    instance["audioTrackName"] = instance["hasAudioTrack"][0]["trackName"]
                    del instance["hasAudioTrack"]
                    MBRecord["itemInstance"].append(instance)
                except IndexError:
                    logging.warning("Missing Audio Track for " + recordSetId + '_'+ edge["identifier"]  + '_' + MBRecord["itemID"])

            # Parts
            MBRecord["itemPart"] = []
            for part in item["hasPart"]:
                MBRecord["itemPart"].append(part)

            # Streams URN ° TODO!!!!
            #for EdgeRelEdObj in edge["hasRelatedEditorialObject"]:
            #    if "hasMember" in EdgeRelEdObj:
            #        for IssueMember in EdgeRelEdObj["hasMember"]:
            #            if [element for element in EdgeRelEdObj["hasMember"] if element['title'].translate(str.maketrans('', '', '\\')) ==     MBRecord["title"].translate(str.maketrans('', '', '\\'))]:
            #                MBRecord["streamingLink"] = next(element for element in EdgeRelEdObj["hasMember"] if element["title"].translate(str.    maketrans('', '', '\\')) == MBRecord["title"].translate(str.maketrans('', '', '\\')))["identifier"]

            # Add technical remarks
            MBRecord["technicalRemarks"] = "Programm ID: " + MBRecord["issueID"] + "<br/>Item ID: " + MBRecord["itemID"] + "<br/>Beitrag: " +     str(positionInt)

            # Add backlink
            if "itemURN" in MBRecord and MBRecord["itemURN"] is not None:
                MBRecord["references"] = "https://www." + institutitionId + ".ch/play/tv/redirect/detail/" + MBRecord["itemURN"] # ° CHECK IF MAKE ELSE

            # Set access
            try:
                if MBRecord["streamingLink"] is not None:
                    MBRecord["access"] = "public" 
                    #item["accessFaro"] = "faro"
                else:
                    MBRecord["access"] = "onsite"
            except KeyError:
                logging.warning("No streaming link for " + recordSetId + '_'+ edge["identifier"]  + '_' + MBRecord["itemID"])

            ### OUTPUT DEFINITION
            with open(os.path.join(output_folder, f'{recordSetId}_{MBRecord["MemobaseID"]}.json'), 'w') as fp:
                json.dump(MBRecord, fp, ensure_ascii=False, indent=None, separators=(',', ':'))
    ### CUSTOM CODE END

### TEMPLATE WRAPPER CODE. DO NOT CHANGE THIS.
def main(message: str, institutionId: str, recordSetId: str):
    try:
        document = json.loads(message)
        try:
            transformation(document, institutionId, recordSetId)
        except Exception as er:
            logging.error(er)
            exit(1)
    except JSONDecodeError as er:
        logging.error(f'INVALID JSON (Could not deserialize input as JSON): {er}.')
        exit(1)


if __name__ == '__main__':
    with open(sys.argv[1], 'r') as fp:
        message = fp.read()
    main(message, sys.argv[2], sys.argv[3])
