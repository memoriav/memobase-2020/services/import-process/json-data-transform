/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.memobase

object BranchNames {


    const val SFTP_CHECK_SCRIPT = "sftp-check-script-"
    const val SFTP_CHECK_CONFIG = "sftp-check-config-"
    const val API_CHECK_SCRIPT = "api-check-script-"
    const val API_CHECK_CONFIG = "api-check-config-"

    const val CONFIG_CHECK_FAILED = "failed"
    const val CONFIG_CHECK_SUCCESS = "success"

    const val SPLIT_SFTP = "sftp"
    const val SPLIT_SFTP_FAIL = "fail"
    const val SPLIT_SFTP_SUCCESS = "success"
}