/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.BranchNames.API_CHECK_CONFIG
import ch.memobase.BranchNames.API_CHECK_SCRIPT
import ch.memobase.BranchNames.CONFIG_CHECK_FAILED
import ch.memobase.BranchNames.CONFIG_CHECK_SUCCESS
import ch.memobase.BranchNames.SFTP_CHECK_CONFIG
import ch.memobase.BranchNames.SFTP_CHECK_SCRIPT
import ch.memobase.BranchNames.SPLIT_SFTP
import ch.memobase.BranchNames.SPLIT_SFTP_FAIL
import ch.memobase.BranchNames.SPLIT_SFTP_SUCCESS
import ch.memobase.exceptions.SftpClientException
import ch.memobase.kafka.utils.ConfigJoiner
import ch.memobase.kafka.utils.models.ImportService
import ch.memobase.kafka.utils.models.JoinedValues
import ch.memobase.kafka.utils.models.ValueWithException
import ch.memobase.reporting.Report
import ch.memobase.reporting.ReportStatus
import ch.memobase.settings.HeaderExtractionSupplier
import ch.memobase.settings.SettingsLoader
import ch.memobase.utility.AcceptedFileFormat
import org.apache.kafka.common.serialization.Serdes
import org.apache.kafka.streams.KeyValue
import org.apache.kafka.streams.StreamsBuilder
import org.apache.kafka.streams.Topology
import org.apache.kafka.streams.kstream.Branched
import org.apache.kafka.streams.kstream.KStream
import org.apache.kafka.streams.kstream.Named
import org.apache.logging.log4j.LogManager

class KafkaTopology(settings: SettingsLoader) {
    private val log = LogManager.getLogger(this::class.java)

    private val step = settings.appSettings.getProperty(PropertyName.REPORTING_STEP_NAME)
    private val stepVersion = settings.appSettings.getProperty(PropertyName.APP_VERSION)
    private val srgApiIngestTopic = settings.appSettings.getProperty(PropertyName.SRG_API_INGEST_TOPIC)
    private val configTopic = settings.appSettings.getProperty(PropertyName.CONFIG_TOPIC_NAME)
    private val scriptOutputFolder = settings.appSettings.getProperty(PropertyName.SCRIPT_OUTPUT_FOLDER)
    private val scriptInputFolder = settings.appSettings.getProperty(PropertyName.SCRIPT_INPUT_FOLDER)
    private val inputTopic = settings.inputTopic
    private val outputTopic = settings.outputTopic
    private val reportTopic = settings.processReportTopic

    private val reports = Reports(step, stepVersion)

    private val jsonHandler = JsonHandler()
    private val pythonHandler = PythonHandler(reports, scriptInputFolder, scriptOutputFolder)
    private val sftpHandler = SftpHandler(settings.sftpSettings)

    private val pythonTransformJoiner =
        ConfigJoiner(
            ImportService.PythonTransform,
            Serdes.String(),
            Serdes.serdeFrom({ _, data -> data }, { _, data -> data }), this::parseConfig
        )
    private val pythonTransformConfigJoiner =
        ConfigJoiner(
            ImportService.PythonTransformConfig,
            Serdes.String(),
            Serdes.serdeFrom({ _, data -> data }, { _, data -> data }), this::parseConfig
        )


    fun build(): Topology {
        val builder = StreamsBuilder()
        val sftpInputStream = buildSftpJsonIngestPipeline(builder)
        val srgApiInputStream = buildSrgApiIngestPipeline(builder)

        buildScriptPipeline(sftpInputStream, builder, SFTP_CHECK_SCRIPT, SFTP_CHECK_CONFIG)
        buildScriptPipeline(srgApiInputStream, builder, API_CHECK_SCRIPT, API_CHECK_CONFIG)
        return builder.build()
    }

    private fun buildSftpJsonIngestPipeline(builder: StreamsBuilder): KStream<String, String>? {
        val parseSftpFile =
            builder.stream<String, String>(inputTopic).mapValues { value -> jsonHandler.parseMessage(value) }
                .filter { _, value ->
                    value.format == AcceptedFileFormat.JSON
                }
                .mapValues { value ->
                    try {
                        val content = sftpHandler.open(value.path).use { inputStream ->
                            inputStream.bufferedReader(Charsets.UTF_8).use {
                                it.readText()
                            }
                        }
                        PipelineInput(content, "")
                    } catch (ex: SftpClientException) {
                        PipelineInput("", ex.localizedMessage)
                    }
                }
                .split(Named.`as`(SPLIT_SFTP))
                .branch(
                    { _, value -> value.message.isNotEmpty() },
                    Branched.`as`(SPLIT_SFTP_SUCCESS)
                )
                .defaultBranch(Branched.`as`(SPLIT_SFTP_FAIL))

        parseSftpFile["$SPLIT_SFTP$SPLIT_SFTP_FAIL"]
            ?.mapValues { readyOnlyKey, value ->
                if (value.message.isEmpty() && value.errorMessage.isEmpty()) {
                    reports.fatal(readyOnlyKey, "Input Exception: the input file is empty.").toJson()
                } else {
                    reports.fatal(readyOnlyKey, "SFTP Exception: ${value.errorMessage}.").toJson()
                }
            }
            ?.to(reportTopic)

        return parseSftpFile["$SPLIT_SFTP$SPLIT_SFTP_SUCCESS"]
            ?.mapValues { value -> value.message }
    }

    private fun buildSrgApiIngestPipeline(builder: StreamsBuilder): KStream<String, String> {
        return builder.stream(srgApiIngestTopic)
    }

    private fun buildScriptPipeline(
        stream: KStream<String, String>?,
        builder: StreamsBuilder,
        checkScript: String,
        checkConfig: String
    ) {
        stream?.filter { _, value -> value == "{}" }
            ?.mapValues { readOnlyKey, _ -> reports.fatal(readOnlyKey, "Empty json object in input.").toJson() }
            ?.to(reportTopic)

        val filteredStream = stream
            ?.filter { _, value -> value != "{}" }

        val configStream = builder.stream<String, String>(configTopic)
            .map { key, value -> KeyValue(key.toByteArray(), value.toByteArray()) }

        val withPythonScript =
            joinWithConfigStream(
                filteredStream,
                pythonTransformJoiner,
                configStream,
                checkScript
            )

        val flattenStream = withPythonScript
            ?.mapValues { value ->
                SrgApiPipelineData(value.config.decodeToString(), "", value.message).toJson()
            }

        val withOptionalPythonScriptConfig = joinWithConfigStream(
            flattenStream, pythonTransformConfigJoiner, configStream, checkConfig
        )

        val transformedStream = withOptionalPythonScriptConfig
            ?.mapValues { value ->
                val data = jsonHandler.parseSrgApiPipelineData(value.message)
                value.let {
                    if (it.config.isEmpty()) {
                        SrgApiPipelineData(data.pythonScript, "", data.message)
                    } else {
                        SrgApiPipelineData(data.pythonScript, value.config.decodeToString(), data.message)
                    }
                }

            }
            ?.processValues(HeaderExtractionSupplier<SrgApiPipelineData>())
            ?.mapValues { readOnlyKey, value ->
                val data = value.first
                val metadata = value.second
                if (data.pythonScript.isNotEmpty()) {
                    pythonHandler.transform(
                        readOnlyKey,
                        data.pythonScript,
                        data.pythonScriptData,
                        data.message,
                        metadata.institutionId,
                        metadata.recordSetId
                    )
                } else {
                    PipelineOutput(
                        listOf(jsonHandler.normalizeMessage(data.message)),
                        Report(
                            readOnlyKey,
                            ReportStatus.warning,
                            "No changes made to the JSON file.",
                            step,
                            stepVersion,
                        )
                    )
                }

            }

        transformedStream
            ?.mapValues { value -> value.report.toJson() }
            ?.to(reportTopic)

        transformedStream
            ?.filter { _, value -> value.report.status != ReportStatus.fatal }
            ?.flatMapValues { value -> value.results }
            ?.to(outputTopic)
    }

    private fun joinWithConfigStream(
        stream: KStream<String, String>?,
        joiner: ConfigJoiner<String, ByteArray?>,
        configStream: KStream<ByteArray, ByteArray>,
        branchName: String,
    ): KStream<String, PipelineExceptionOutput>? {
        val joinedStream = joiner.join(stream, configStream)

        val handledStream = joinedStream
            .mapValues { value ->
                handleExceptions(value)
            }
            .split(Named.`as`(branchName))
            .branch(
                { _, value -> value.errorMessage != "" }, Branched.`as`(CONFIG_CHECK_FAILED)
            )
            .defaultBranch(Branched.`as`(CONFIG_CHECK_SUCCESS))

        handledStream["${branchName}${CONFIG_CHECK_FAILED}"]
            ?.mapValues { readOnlyKey, value ->
                log.error("CONFIGURATION ERROR: ${value.errorMessage}")
                reports.fatal(readOnlyKey, "CONFIGURATION ERROR: ${value.errorMessage}").toJson()
            }
            ?.to(reportTopic)

        return handledStream["${branchName}${CONFIG_CHECK_SUCCESS}"]
    }

    private fun handleExceptions(
        value: ValueWithException<JoinedValues<String, ByteArray?>>
    ): PipelineExceptionOutput {
        return when {
            value.hasException() -> {
                PipelineExceptionOutput("", ByteArray(0), value.exception.localizedMessage)
            }

            value.hasValue() -> {
                val bytes = value.value.right
                bytes.let { content ->
                    if (content == null || content.isEmpty()) {
                        PipelineExceptionOutput(value.value.left, ByteArray(0), "")
                    } else {
                        PipelineExceptionOutput(value.value.left, content, "")
                    }
                }
            }

            else -> {
                PipelineExceptionOutput("", ByteArray(0), "Could not handle error in kafka utils library.")
            }
        }
    }

    private fun parseConfig(data: ByteArray): ByteArray {
        return data
    }
}
