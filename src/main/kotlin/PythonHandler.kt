/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.memobase

import org.apache.logging.log4j.LogManager
import java.io.*
import java.nio.charset.Charset

class PythonHandler(
    private val reports: Reports,
    scriptInputFolder: String,
    scriptOutputFolder: String
) {
    private val log = LogManager.getLogger(this::class.java)
    private val python = "python3"
    private val scriptName = "python_script.py"
    private val dataFileName = "json_transform_configs.txt"
    private val messageFileName = "message.json"
    private val basePathInput = File(scriptInputFolder)
    private val basePathOutput = File(scriptOutputFolder)

    private val scriptFile = File(basePathInput, scriptName)
    private val messageFile = File(basePathInput, messageFileName)
    private val dataFile = File(basePathInput, dataFileName)

    init {
        try {
            basePathInput.mkdirs()
        } catch (ex: IOException) {
            log.error("Could not create the input folders for the python scripts: ${ex.message}.")
        }
        try {
            basePathOutput.mkdirs()
        } catch (ex: IOException) {
            log.error("Could not create the output folders for the python scripts: ${ex.message}.")
        }
    }

    fun transform(
        key: String,
        pythonScript: String,
        pythonTransformConfig: String,
        message: String,
        institutionId: String,
        recordSetId: String
    ): PipelineOutput {
        try {
            BufferedWriter(FileWriter(messageFile)).use {
                it.write(message)
            }
            log.info("Created ${messageFile.path}.")
        } catch (ex: IOException) {
            log.error("Failed to create input file: ${ex.message}.")
            return PipelineOutput(
                listOf(),
                reports.fatal(
                    key,
                    "Failed to create input file: ${ex.message}."
                )
            )
        }

        try {
            BufferedWriter(FileWriter(scriptFile)).use {
                it.write(pythonScript)
            }
            log.info("Created ${scriptFile.path}.")
        } catch (ex: IOException) {
            log.error("Failed to create python script: ${ex.message}.")
            return PipelineOutput(
                listOf(),
                reports.fatal(
                    key,
                    "Failed to create python script: ${ex.message}."
                )
            )
        }

        try {
            BufferedWriter(FileWriter(dataFile)).use {
                it.write(pythonTransformConfig)
            }
            log.info("Created ${dataFile.path}.")
        } catch (ex: IOException) {
            log.error("Failed to create data file: ${ex.message}.")
            return PipelineOutput(
                listOf(), reports.fatal(
                    key,
                    "Failed to create data file: ${ex.message}."
                )
            )
        }

        // delete the old output files. This is necessary as they do not have the same names. Otherwise, they start to
        // pile up until the disk is full.
        val failedToDelete = mutableListOf<String>()
        basePathOutput.walk().forEach { file ->
            try {
                if (file.isFile) {
                    file.delete()
                }
            } catch (ex: Exception) {
                log.error("Could not delete the old output files: ${ex.message}.")
                failedToDelete.add("Failed to delete output file $file.")
            }
        }

        return try {

            val pb = ProcessBuilder(python, scriptFile.path, messageFile.path, institutionId, recordSetId)
            val p = pb.start()
            // IMPORTANT
            // The process does not end until the standard output is fully read. So it has to always be
            // read before the error output. Otherwise, if the error output is empty the process gets stuck and never
            // ends. So do not change the below order of lines.
            val standardOutput = BufferedReader(InputStreamReader(p.inputStream))
            val warnings = standardOutput.use {
                it.readLines().joinToString("\n")
            }
            val errorStream = BufferedReader(InputStreamReader(p.errorStream))
            val errorOutput = errorStream.use {
                it.readLines().joinToString(separator = "\n")
            }
            // IMPORTANT to wait for this to finish to make sure that the process has actually ended before
            // the next one is started!
            p.waitFor()
            val reportMessages = errorOutput + "\n" + warnings + "\n" + failedToDelete.joinToString(separator = "\n")
            val records = basePathOutput.walk().filter { file -> file.isFile }.mapNotNull { file ->
                file.inputStream().use { fileInputStream ->
                    fileInputStream.bufferedReader(Charset.defaultCharset()).use { bufferedReader ->
                        bufferedReader.readLine()
                    }
                }
            }
                .toList()
            val numberOfRecords = records.size
            val report = if (errorOutput.isNotEmpty()) {
                reports.fatal(
                    key,
                    "The python script failed with an error. Total Number of Records: $numberOfRecords.\n" +
                            "Error Messages:\n$reportMessages"
                )
            } else if (numberOfRecords == 0) {
                reports.fatal(
                    key,
                    "The python script did not produce any records from the input message.\n" +
                            "Error Messages:$reportMessages"
                )
            } else if (warnings.isNotEmpty() || failedToDelete.isNotEmpty()) {
                reports.warning(
                    key,
                    "The python script finished properly but had some errors. " +
                            "Total Number of Records: $numberOfRecords.\n" +
                            "Warnings:\n$reportMessages",
                )
            } else {
                reports.success(
                    key,
                    "All records were generated without warnings! Total Number of Records: $numberOfRecords."
                )
            }
            PipelineOutput(records, report)
        } catch (ex: Exception) {
            log.error("Unknown Exception (${ex::class.java.name}): ${ex.message}.\nCommand: $python ${scriptFile.path} ${messageFile.path} $institutionId $recordSetId")
            PipelineOutput(
                listOf(), reports.fatal(
                    key,
                    "Unknown Exception (${ex::class.java.name}): ${ex.message}.\n" +
                            "Command: $python ${scriptFile.path} ${messageFile.path} $institutionId $recordSetId"
                )
            )
        }
    }
}