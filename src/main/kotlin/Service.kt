/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.PropertyName.APP_VERSION
import ch.memobase.PropertyName.CONFIG_TOPIC_NAME
import ch.memobase.PropertyName.REPORTING_STEP_NAME
import ch.memobase.PropertyName.SCRIPT_INPUT_FOLDER
import ch.memobase.PropertyName.SCRIPT_OUTPUT_FOLDER
import ch.memobase.PropertyName.SRG_API_INGEST_TOPIC
import ch.memobase.settings.SettingsLoader
import org.apache.kafka.streams.KafkaStreams
import org.apache.logging.log4j.LogManager

class Service {
    private val log = LogManager.getLogger(this::class.java)

    companion object {
        fun load(filename: String): SettingsLoader {
            return SettingsLoader(
                listOf(
                    REPORTING_STEP_NAME,
                    SRG_API_INGEST_TOPIC,
                    CONFIG_TOPIC_NAME,
                    SCRIPT_INPUT_FOLDER,
                    SCRIPT_OUTPUT_FOLDER,
                    APP_VERSION,
                ),
                filename,
                useStreamsConfig = true,
                readSftpSettings = true
            )
        }

        val settings = load("app.yml")
    }

    private val topology = KafkaTopology(settings)
    private val stream = KafkaStreams(topology.build(), settings.kafkaStreamsSettings)

    fun run() {
        try {
            stream.start()
            while (stream.state().isRunningOrRebalancing) {
                Thread.sleep(10_000L)
            }
            stream.close()
        } catch (ex: Exception) {
            log.error("Unknown Exception: ${ex.localizedMessage}.")
            stream.close()
        }
    }
}