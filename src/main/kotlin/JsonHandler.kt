/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.utility.AcceptedFileFormat
import ch.memobase.utility.TextFileValidationMessage
import kotlinx.serialization.SerializationException
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonElement

class JsonHandler {

    private val json = Json {
        prettyPrint = false
    }

    fun parseMessage(data: String): TextFileValidationMessage {
        return try {
            json.decodeFromString<TextFileValidationMessage>(data)
        } catch (ex: SerializationException) {
            TextFileValidationMessage(AcceptedFileFormat.ERROR, "JSON Exception: ${ex.localizedMessage}.")
        }
    }

    fun parseSrgApiPipelineData(data: String): SrgApiPipelineData {
        return try {
            json.decodeFromString<SrgApiPipelineData>(data)
        } catch (ex: SerializationException) {
            SrgApiPipelineData("", "", "")
        }
    }

    fun normalizeMessage(data: String): String {
        return json.encodeToString(json.decodeFromString<JsonElement>(data))
    }
}