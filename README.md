### Json Data Transform


[Confluence Documentation](https://ub-basel.atlassian.net/wiki/spaces/MEMOBASE/pages/29197035/Service+JSON+Data+Transform)

#### Testing
The tests for the python script pipeline are run in docker.
The relevant Dockerfile to use is `Dockerfile.test`.

In Intellij this is fairly simple to set up. Just configure an image of
that dockerfile to be the target to run on in the Run Configurations.

The configuration files are in the project. They are in the `.run` folder and the `.idea/remote-target.xml` file.
#### Data
The file `srf_029_call_number_translation_data.csv` is used by the record set 
srf-029 in the srg import build path to transform the 
identifiers of the data. 

The file is directly embedded into the container as it is too large for Kafka
messages and too large for Kubernetes ConfigMaps.